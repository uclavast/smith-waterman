# Smith-Waterman

## Developer
JieLEI

jiejielei@gmail.com

## Description
This is a project for accelerating the Smith-Waterman algorithm applied in BWA-MEM. In order to increase the data throughput, a PE-array structure which enables a set of PEs to run in parallel is implemented. Round-robin scheduling is utilized to dispatch a bunch of tasks into PEs dynamically.

## Design Specification
Each PE processes a task sequentially.
A task consists of two pairs of read-reference sequences which represent left-extension and right-extension of reference sequence respectively.
Each DNA character is coded in 4-bit.
Both of top input and output data channels have a bit width of 32-bit. 

## Code Navigation
mmult(): top function of the kernel. 

  - argument a: input buffer, a number of tasks are organized together, the location of each task is saved in the header of input buffer "a".

  - argument output_a: output buffer, 5 integers returned as the results of each task, the first integer stand for the index number of the task.

  - argument __inc: input, indicate the total number of tasks required to be processed.

data_parse(): copy some number of tasks from DRAM into local block RAMs. It is able to manage several independent local RAM buffers. Each batch-task RAM buffer can store a batch of tasks for a PE array to process.

proc_block(): fetch each task from batch-task RAM buffer and feed it to a input FIFO of a PE which has space to accomodate the task data.

proc_element(): PE unit. Process the tasks one by one.

receive_match(): collect results from each PE associated to a certain PE array and pack them into a local batch-result RAM buffer.

results_assemble(): collect results from each batch-result RAM buffer and pack them together to a result output buffer.

upload_results(): memory copy the results from the output buffer to the off-chip DRAM. 

## Test Instruction
  - Input test data is saved in a file named as "total_input.dat". Its location is "/curr/jielei/cmost_lib_flow/picasso_flow/Test_Data/total_input.dat". The current "total_input.dat" file contains 12288 tasks. You can use other input test files named as "total_input_*.dat" where '*' represents the number of tasks that file has. You can also change the test file path and name in the openCL host file "test-cl.c".(202L)

  - Ouput result data is stored in a file name as "results.dat". Its path and name are allowed to alter as well in the openCL host file "test-cl.c".(203L)

  - The golden reference result data files are located in "/curr/jielei/cmost_lib_flow/picasso_flow/Test_Data" named as "result_golden_*.dat".

  - Set the number of PEs in a PE-Array by defining the value of "PE_NUMS". Set the number of PE-Arrays by setting the value of "PEARRAY_NUM". (In mmult1.cpp, 61L and 62L).

  - Invoke SDAccel to run the TCL file of "dramBandwidth_improve.tcl". The whole system will be built, then you can run the hardware test by executing "./dramBandwidth_improve.exe mmult1.xclbin" under the directory "./dramBandwidth_improve/pkg/pcie/".

  - Compare the new generated result data file to the golden reference result data file to check if the hardware test complete successfully.

## Test Results

- **Environment Setup**
    - Xilinx SDAccel 2015.1.5
    - AlphaData card

- **Performance**
    - Some results

- **Throughput**
    - Some results
