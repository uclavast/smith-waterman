# Accelerator Code Review: Smith-Waterman

## Reviewers
- Cody Hao Yu, hyu@cs.ucla.edu
- Peipei Zhou, memoryzpp@cs.ucla.edu

## Summary
- Cody
	- Well-organized code structure.
	- A great efficient design using data streaming and FIFO.
	- I personally learned a lot about the right coding styles of HLS to make an efficient design.
		- Dataflow, streaming.
		- Lots of usages of HLS pragmas such as LOOP_TRIPCOUNT, ARRAY_MAP, RESOURCE.
		
- Peipei
	- The Code is well written and wrapped in different functions
	- The input feed data function, process element function, output data collect functions are clearly separated.

## Problems and suggestions
- Cody
	- Lack comments in the source file. Each function should have the description and each phase of the computation should have the explanation as well. This is important especially for the core Smith-Waterman algorithm since it has lots of computations and really hard to figure out the meaning even for the developer.
	- The reliability of HLS streaming highly depends on the implementation of Xilinx tools and drivers. It's easy to get into a trap but hard to be verified.
	- What is the size of the FIFO between each stage? Theoretically, II=1 can be achieved only when the size of FIFOs is enough, but the latency of data transfer may have variations. This problem not only degrades the performance, but causes FIFO overflow or empty, and further affect the reliability of the design.
	- Here is two `AP dependence array inter false` pragmas in the core function `sw_extend`. This pragma should be used very carefully because it may cause the incorrect results sometimes. 
	- The pragma `LOOP_TRIPCOUNT` is only used for the report, and it has no impact on synthesis according to the Vivado user manual. However, I found this pragma is used in a fixed bound loop (line 435). we should use `HLS UNROLL` instead.
	
- Peipei
	- In the Design Specification, the pipeline II of each function is not specified and should be added. 
	- How the stream FIFO depth is determined in the pragma? Are there good ways to find the size systematically?
